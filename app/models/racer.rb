require "mongo"
class Racer
	include ActiveModel::Model
	
	attr_accessor :id, :number, :first_name, :last_name, :gender, :group, :secs
	
	# return MongoDB client
	def self.mongo_client
		Mongoid::Clients.default
	end
	
	# return racers collection
	def self.collection
		mongo_client[:racers]
	end
	
	# return all racers matching the criteria
	def self.all(prototype={}, sort={}, skip=0, limit=nil)
		result = collection.find(prototype)
			.sort(sort)
			.skip(skip)
		if !limit
			result
		else
			result.limit(limit)
		end
	end
	
	# initialise Racer instance
	def initialize(params={})
		@id = params[:_id].nil? ? params[:id] : params[:_id].to_s
		@number = params[:number].to_i
		@first_name = params[:first_name]
		@last_name = params[:last_name]
		@gender = params[:gender]
		@group = params[:group]
		@secs = params[:secs].to_i
	end
	
	# return instance of a Racer
	def self.find id
		result = collection.find({"_id": BSON::ObjectId.from_string(id)}).first
		return result.nil? ? nil : Racer.new(result)
	end
	
	# save current Racer
	def save
		result = self.class.collection.insert_one(
			number: @number,
			first_name: @first_name,
			last_name: @last_name,
			gender: @gender,
			group: @group,
			secs: @secs
		)
		@id = result.inserted_id.to_s
	end
	
	# update current Racer
	def update(params)
		@number = params[:number].to_i
		@first_name = params[:first_name]
		@last_name = params[:last_name]
		@gender = params[:gender]
		@group = params[:group]
		@secs = params[:secs].to_i
		params.slice!(:number, :first_name, :last_name, :gender, :group, :secs)
		self.class.collection.find(:_id => BSON::ObjectId.from_string(@id))
			.update_one(params)
	end
	
	# destroy Racer associated with current @number
	def destroy
		self.class.collection.find({number: @number}).delete_one
	end
	
	# return true when @id is not nil
	def persisted?
		!@id.nil?
	end
	
	# return created_at - nil as a placeholder
	def created_at
		nil
	end
	
	# return updated_at - nil as a placeholder
	def updated_at
		nil
	end
	
	# paginate results
	def self.paginate(params)
		page = (params[:page] || 1).to_i
		limit = (params[:per_page] || 30).to_i
		skip = (page - 1) * limit
		sort = {"number" => 1}
		racers = []
		all({}, sort, skip, limit).each do |doc|
			racers << Racer.new(doc)
		end
		total = all().count
		
		WillPaginate::Collection.create(page, limit, total) do |pager|
			pager.replace(racers)
		end
	end
end